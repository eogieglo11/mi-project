class CreateTweets < ActiveRecord::Migration[7.0]
  def change
    create_table :tweets do |t|
      t.integer :id
      t.string :details

      t.timestamps
    end
  end
end
